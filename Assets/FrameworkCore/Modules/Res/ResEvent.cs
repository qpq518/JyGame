﻿/*
 *  date: 2018-07-13
 *  author: John-chen
 *  cn: 资源操作事件
 *  en: todo:
 */

namespace JyFramework
{
    /// <summary>
    /// 资源操作事件
    /// </summary>
    public sealed class ResEvent
    {
        /// <summary>
        /// 成功加载AssetBundle后添加
        /// </summary>
        public static string AddAssetBundleRes { get { return "AddAssetBundleResEvent";} }

        /// <summary>
        /// 添加AssetBundle的引用
        /// </summary>
        public static string AddAssetBundleResRef { get { return "AddAssetBundleResRefEvent";} }

        /// <summary>
        /// 减少AssetBundle的引用
        /// </summary>
        public static string ReduceAssetBundleResRef { get { return "ReduceAssetBundleResRefEvent"; } }

        /// <summary>
        /// 被引用的AssetBundle完全删除
        /// </summary>
        public static string DestoryRefAssetBundle { get { return "DestoryRefAssetBundleEvent";} }

        /// <summary>
        /// 添加引用
        /// </summary>
        public static string OnAddRef { get { return "OnAddRef"; } }

        /// <summary>
        /// 减少引用
        /// </summary>
        public static string OnReduceRef { get { return "OnReduceRef"; } }
    }
}