﻿/*
 *  date: 2018-03-27
 *  author: John-chen
 *  cn: 日志模块
 *  en: todo:
 */

namespace JyFramework
{
    /// <summary>
    /// 日志模块
    /// </summary>
    public class LogModule : BaseModule
    {
        /// <summary>
        /// 日志类
        /// </summary>
        public JyLog Log { get { return _log; } }

        public LogModule(string name = ModuleName.Log) : base(name)
        {
            
        }

        public override void InitGame()
        {
            _log = new JyLog();
        }

        private JyLog _log;
    }
}
