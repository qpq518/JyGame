﻿/*
 *  date: 2018-05-18
 *  author: John-chen
 *  cn: 人工智能模块
 *  en: todo:
 */

namespace JyFramework
{
    /// <summary>
    /// 人工智能模块
    /// </summary>
    public class AIModule : BaseModule
    {
        public AIModule(string name = ModuleName.Aduio) : base(name)
        {

        }

        public override void InitGame()
        {
            
        }
    }
}
