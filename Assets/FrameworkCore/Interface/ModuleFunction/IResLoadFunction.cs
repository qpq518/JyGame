﻿/*
 *  date: 2018-06-29
 *  author: John-chen
 *  cn: 资源加载接口
 *  en: todo:
 */


using UnityEngine;
using System.Collections;
using Object = UnityEngine.Object;

namespace JyFramework
{
    /// <summary>
    /// 异步实例化委托
    /// </summary>
    /// <param name="obj"></param>
    public delegate void AsyncInstantiateHandler(GameObject obj);

    /// <summary>
    /// 异步加载成功委托
    /// </summary>
    /// <param name="ab"></param>
    public delegate void AsyncLoadedHandle(AssetBundle ab);

    /// <summary>
    /// 资源加载接口
    /// </summary>
    public interface IResLoadFunction
    {
        /// <summary>
        /// 同步加载 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        T LoadRes<T>(string path) where T : Object;

        /// <summary>
        /// 通过异步返回加载资源
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="yieldArgs"></param>
        /// <returns></returns>
        T LoadRes<T>(YieldArgs yieldArgs) where T : Object;

        /// <summary>
        /// 协程异步加载
        /// </summary>
        /// <param name="yieldArgs"></param>
        /// <returns></returns>
        IEnumerator AsyncLoadRes(YieldArgs yieldArgs, AsyncLoadedHandle action);

        /// <summary>
        /// 实例化
        /// </summary>
        /// <param name="yieldArgs"></param>
        /// <returns></returns>
        GameObject Instantiate(YieldArgs yieldArgs);

        /// <summary>
        /// 异步实例化
        /// </summary>
        /// <param name="yieldArgs"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        IEnumerator AsyncInstantiate(YieldArgs yieldArgs, AsyncInstantiateHandler action);
    }
}